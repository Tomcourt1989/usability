import apache_log_parser

from csv import DictWriter

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s %b')

with open('cleaned_log.csv', 'w') as out_f:
    writer = DictWriter(out_f,
                        fieldnames=['remote_host',
                                    'time_received_datetimeobj',
                                    'request_method',
                                    'request_url',
                                    'request_http_ver',
                                    'status',
                                    'response_bytes_clf'],
                        extrasaction='ignore')
    writer.writeheader()

    ip_map = {}
    with open('logs/sad-access_log') as in_f1, open('logs/sad-access_log-20180902') as in_f2, open('logs/sad-access_log-20180909') as in_f3, open('logs/sad-access_log-20180916') as in_f4, open('logs/sad-access_log-20180923') as in_f5:
        for line in in_f1:
            line = parser(line)

            ip_addr = line['remote_host']
            if ip_addr not in ip_map:
                ip_map[ip_addr] = str(len(ip_map) + 1)
            line['remote_host'] = ip_map[ip_addr]
            writer.writerow(line)
        for line in in_f2:
            line = parser(line)
            writer.writerow(line)

        for line in in_f3:
            line = parser(line)
            writer.writerow(line)

        for line in in_f4:
            line = parser(line)
            writer.writerow(line)
            
        for line in in_f5:
            line = parser(line)
            writer.writerow(line)
            
