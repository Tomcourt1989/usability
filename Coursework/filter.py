import csv
import pandas as pd
import os, re
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from collections import Counter


df = pd.read_csv('cleaned_log.csv')

# remove excess filetypes
df = df[~df['request_url'].str.contains('.jpg')]
df = df[~df['request_url'].str.contains('.gif')]
df = df[~df['request_url'].str.contains('.png')]
df = df[~df['request_url'].str.contains('.svg')]
df = df[~df['request_url'].str.contains('.woff')]
df = df[~df['request_url'].str.contains('.css')]
df = df[~df['request_url'].str.contains('.eot')]
df = df[~df['request_url'].str.contains('.js')]
df = df[~df['request_url'].str.contains('.flv')]
df = df[~df['request_url'].str.contains('.mp3')]
df = df[~df['request_url'].str.contains('.ttf')]
df = df[~df['request_url'].str.contains('.swf')]
df = df[~df['request_url'].str.contains('.JPG')]
df = df[~df['request_url'].str.contains('.pdf')]
# removing the robots and favicon file requests
df = df[~df['request_url'].str.contains('robots.txt')]
df = df[~df['request_url'].str.endswith('.ico')]
# removing head requests and auth tokens
df = df[~df['request_method'].str.contains('HEAD')]
df = df[~df['request_url'].str.contains('authenticity_token')]
# removing admin requests and page requests to registered users
df = df[~df['request_url'].str.contains('.php')]
df = df[~df['request_url'].str.contains('/users')]
df = df[~df['request_url'].str.contains('/courses_users')]

df.to_csv('filtered.csv')

# Create a dataframe with only successful responses
scs = df.drop(df[(df.status != 200)].index)
scs.to_csv('successes.csv')
# scs.info()

# Create a dataframe with only partial success responses
prtl = df.drop(df[(df.status != 206)].index)
# prtl.info()

# Create a dataframe with only redirects
rdt = df.drop(df[(df.status != 301) & (df.status != 302) & (df.status != 304)].index)
# rdt.info()

# Create a dataframe with only failed responses
fls = df.drop(df[(df.status != 400) & (df.status != 403) & (df.status != 404)].index)
# fls.info()

# Create a dataframe with only server errors
err = df.drop(df[(df.status != 500) & (df.status != 502)].index)
# err.info()


# Counts

# Count successful requests to see most popular pages
total = scs.shape[0]
# print(total, 'Successful Rows')
scscounts = scs['request_url'].value_counts()
scscounts = scscounts[:20]
# print(scscounts)

# Count failed requests to see which pages are failing the most
total = fls.shape[0]
# print(total, 'Failed Rows')
flscounts = fls['request_url'].value_counts()
flscounts = flscounts[:20]
# print(flscounts)

# Count server errors
total = err.shape[0]
# print(total, 'Error Rows')
errcounts = err['request_url'].value_counts()
errcounts = errcounts[:20]
# print(errcounts)


# Plots

# Most popular pages
# scsplot = scs['request_url'].value_counts()
# scsplot.to_csv('frequencies.csv')
# scsplot = scsplot[:20]
# plt.figure(figsize=(10,5))
# sns.barplot(scsplot.index,scsplot.values,alpha=0.8)
# plt.title('Top 20 Accessed Pages')
# plt.ylabel('Number of Page requests', fontsize=12)
# plt.xlabel('URLS', fontsize=12)
# plt.xticks(rotation=90)
# plt.show()

# Most popular day of the week
# df2 = pd.read_csv('successes.csv', parse_dates=['time_received_datetimeobj'])
# df2['datetime'] = pd.to_datetime(df2['time_received_datetimeobj'], format='%d/%m/%Y %H:%M:%S')
# df2['weekday'] = df2['datetime'].apply(lambda x: x.isoweekday())
# weekdays = df2.groupby('weekday')['remote_host'].agg(len)
# weekdays = weekdays.divide(weekdays.sum())
# weekdays.index = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
# weekdays.plot(kind='barh')
# plt.title('Most popular day of the week')
# plt.xlabel('Visits')
# plt.show()

# Most popular hour of the day
# df3 = pd.read_csv('successes.csv', parse_dates=['time_received_datetimeobj'])
# df3['datetime'] = pd.to_datetime(df3['time_received_datetimeobj'], format='%d/%m/%Y %H:%M:%S')
# df3['hour'] = df3['datetime'].dt.hour
# hour = df3.groupby('hour')['remote_host'].agg(len)
# hour = hour.divide(hour.sum())
# hour.index = ['12am','1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm']
# hour.plot(kind='line')
# plt.title('Most popular hour of the day')
# plt.xlabel('Visits')
# plt.show()