// Demonstration purpose code

function openNav() {
    document.getElementById("menu").style.display = "block";
}
function openSubNav() {
    document.getElementById("subnav").style.display = "block";
}
function closeSubNav() {
    document.getElementById("subnav").style.display = "none";
}
function closeNav() {
    document.getElementById("menu").style.display = "none";
}

function openTaster() {
    document.getElementById("taster").style.display = "block";
}
function closeTaster() {
    document.getElementById("taster").style.display = "none";
}

function openMap() {
    document.getElementById("locate").style.display = "none";
    document.getElementById("taster").style.display = "none";
    document.getElementById("map").style.display = "block";
}

function openAbility() {
    document.getElementById("map").style.display = "none";
    document.getElementById("ability").style.display = "block";
}

function openDate() {
    document.getElementById("ability").style.display = "none";
    document.getElementById("date").style.display = "block";
}

function openTime() {
    document.getElementById("date").style.display = "none";
    document.getElementById("times").style.display = "block";
}